I used Linux to run the project. So using Linux or Mac OS is recommended.
The program applies both queue and priority queue algorithms. 

1. Open terminal and navigate to the folder

2. Type "make all" then a file call "sApp" will be created

3. There are three .in files in the folder and these files will be run as input files with sApp

4. Execute the program by typing "./sApp<simulationShuffled1.in" Here, simulationShuffled1.in can be
   subtituted with simulationShuffled2.in or simulationShuffled3.in

5. You can also change the numbers in .in files if you want to customize your own input. The first 
   column is the arrival time of each customer and the second column is the duration of the process
   time of each customer.